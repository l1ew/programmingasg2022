﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

//============================================================
// Student Number : S10222254, S10221765
// Student Name : Liew Zheng Zhang, Koay Yi Ting
// Module Group : P06
//============================================================

namespace final_prg_asg_2022
{
    class methods
    {
        public static void printMenu()
        {
            Console.WriteLine("\n\n----------------Welcome to Golden Movies----------------\n");
            Console.WriteLine(string.Format("{0,8} {1,30}","", "1. Display all Movies\n"));
            Console.WriteLine(string.Format("{0,12} {1,30}","", "2. Display all movie screenings\n"));
            Console.WriteLine(string.Format("{0,9} {1,30}","", "3. Display a specific movie screenings\n"));
            Console.WriteLine(string.Format("{0,9} {1,30}","", "4. Display all cinemas\n"));
            Console.WriteLine(string.Format("{0,10} {1,30}","", "5. Add a movie screening session\n"));
            Console.WriteLine(string.Format("{0,9} {1,30}","", "6. Delete a movie screening session\n"));
            Console.WriteLine(string.Format("{0,9} {1,30}","", "7. Order movie tickets.\n"));
            Console.WriteLine(string.Format("{0,11} {1,30}","", "8. Cancel order of ticket\n"));
            Console.WriteLine(string.Format("{0,10} {1,30}","", "9. Print list of orders\n"));
            Console.WriteLine(string.Format("{0,12} {1,30}","", "10. Recommend most popular movie\n"));
            Console.WriteLine(string.Format("{0,11} {1,30}", "", "11. Recommend top 3 movies\n"));
            Console.WriteLine(string.Format("{0,10} {1,30}","", "12. Show screenings' seats in order\n"));
            Console.WriteLine(string.Format("{0,2} {1,30}","", "0. Exit.\n"));
            Console.WriteLine("--------------------------------------------------------");
        }

        //return -1 if invalid number option. return numberOption if valid number option from user 
        public static int getNumberOption()
        {
            int numberOption;

            Console.Write("Please enter your option: ");
            while (!int.TryParse(Console.ReadLine(), out numberOption))
            {
                Console.Write("Please enter a valid number: ");
            }
            for (int validNumber = 0; validNumber < 13; validNumber++)  //should not declare 13, declare as const var or have a list of number options, use the size of list
            {
                if (numberOption == validNumber)
                {
                    return numberOption;
                }
            }
            return -1;
        }

        public static List<Screening> loadScreeningDate(List<Screening> screeningDataList, List<Cinema> cinemaDataList, List<Movie> movieDataList)
        {
            int screeningNo = 1001;
            //using StreamReader to read each line of the csv file from desktop
            using (StreamReader eachLines = new StreamReader("C:/Users/zheng/Desktop/Screening.csv"))
            {
                string eachRow;

                _ = eachLines.ReadLine();   //remove header since we already printing header seperately in another method 

                while ((eachRow = eachLines.ReadLine()) != null)   //read each row of string data in csv file until it reach null
                {
                    List<string> stringListOfDetails = eachRow.Split(',').ToList();
                    screeningNo++;
                    foreach (var cinema in cinemaDataList)  //have to cross refer to each cinema in the list of cinemas in order to add Cinema object to Screening argument
                    {
                        foreach (var movie in movieDataList)  //likewise for list of movies
                        {
                            //if Screening Hall number of csv file is same as cinema object hallNo AND if cinema name of csv file is same as cinema object Name AND movie title of csv file same as movie object title,
                            if (stringListOfDetails[3] == cinema.hallNo.ToString() && stringListOfDetails[2] == cinema.name && stringListOfDetails[4] == movie.title)
                            {
                                //add the specific cinema and movie object
                                //CultureInfo.InvariantCulture is used to persist the DateTime data in a culture-independent format.
                                var dateTime = DateTime.ParseExact(stringListOfDetails[0], "dd/MM/yyyy h:mmtt", CultureInfo.InvariantCulture);

                                Screening newScreeningData = new Screening(screeningNo, dateTime, stringListOfDetails[1], cinema, movie);
                                screeningDataList.Add(newScreeningData);
                            }
                        }
                    }
                }
            }
            return screeningDataList;
        }

        public static List<Movie> loadMovieData(List<Movie> movieDataList)
        {
            var callMovieMethods = new Movie();

            //using StreamReader to read each line of the csv file from desktop
            using (StreamReader eachLines = new StreamReader("C:/Users/zheng/Desktop/Movie.csv"))
            {
                string eachRow;
                var movieData = new Movie();
                //DateTime openingDate;

                _ = eachLines.ReadLine();   //remove header of csv file

                while ((eachRow = eachLines.ReadLine()) != null)   //read each row of string data in csv file until it reach null
                {

                    List<string> stringListOfDetails = eachRow.Split(',').ToList();

                    //csv file had some data that includes whitespace and is unreadable in the DateTime format, so if statement is read data that is non-whitespace
                    if (!(stringListOfDetails.Any(string.IsNullOrWhiteSpace)))  //if any of the string list of csv details is not a white space, read the file and add accordingly 
                    {
                        movieData.AddGenre(stringListOfDetails[2]);  //adding row 2 of csv file to each movie's string genreList
                        DateTime openingDate = DateTime.ParseExact(stringListOfDetails[4], "dd/MM/yyyy", CultureInfo.InvariantCulture);  //convert csv datetime detail to DateTime to pass as argument into Movie movieData object

                        movieData = new Movie(stringListOfDetails[0], int.Parse(stringListOfDetails[1]), stringListOfDetails[3], openingDate, movieData.genreList);  //create new object, then add to a list of Movies 
                        movieDataList.Add(movieData);
                    }
                }
            }
            return movieDataList;
        }

        //populate cinema list  
        public static List<Cinema> loadCinemaData(List<Cinema> cinemaDetailsList)
        {
            //using StreamReader to read each line of the csv file from desktop
            using (StreamReader eachLines = new StreamReader("C:/Users/zheng/Desktop/Cinema.csv"))
            {
                string eachRow;

                _ = eachLines.ReadLine();   //remove header since header will be printed in seperate method and header is unreadable in creating object out of it etc

                while ((eachRow = eachLines.ReadLine()) != null)   //read each row of string data in csv file until it reach null
                {
                    List<string> stringListOfDetails = eachRow.Split(',').ToList();

                    var cinemaDetail = new Cinema(stringListOfDetails[0], int.Parse(stringListOfDetails[1]), int.Parse(stringListOfDetails[2]));
                    cinemaDetailsList.Add(cinemaDetail);
                }
            }
            return cinemaDetailsList;
        }

        //print seats of all screenings in descending order
        public static void printSeatsRemainingDescendingOrder(List<Screening> screeningList)
        {
            screeningList.Sort();

            Console.WriteLine("Here are the available screenings' seats remaining in descending order!\n");
            Console.WriteLine(string.Format("{0,-25} {1,-25} {2,-30} {3,-25} {4,-25}", "Screening Cinema Name", "Screening Cinema HallNo", "Screening Seats Remaining", "Screening Date Time", "Movie Name"));
            Console.WriteLine(string.Format("{0,-25} {1,-25} {2,-30} {3,-25} {4,-25}", "---------------------", "-----------------------", "-------------------------", "-------------------", "----------"));
            foreach (var screening in screeningList)
            {
                Console.WriteLine(string.Format("{0,-25} {1,-25} {2,-30} {3,-25} {4,-25}", screening.cinema.name, screening.cinema.hallNo, screening.seatsRemaining, screening.screeningDateTime, screening.movie.title));
            }
        }

        public static void printListOfOrders(List<Order> listOfOrders)
        {
            Console.WriteLine("{0,-20} {1,-30} {2,-20} {3,-20}", "Order No", "Order Date Time", "Amount", "Status");
            Console.WriteLine("{0,-20} {1,-30} {2,-20} {3,-20}", "--------", "---------------", "------", "------");
            foreach (var order in listOfOrders)
            {
                Console.WriteLine(order.ToString());
            }
        }

        public static void printCinemaList(List<Cinema> cinemaList)
        {
            Console.WriteLine("{0,-20} {1,-20} {2,-20}", "Name", "HallNo", "Capacity");
            Console.WriteLine("{0,-20} {1,-20} {2,-20}", "----", "------", "--------");
            foreach (var cinemaDetail in cinemaList)
            {
                Console.WriteLine(cinemaDetail.ToString());
            }
        }

        public static void printMovieList(List<Movie> movieList)
        {
            Console.WriteLine("{0,-30} {1,-20} {2,-20} {3,-20}", "Title", "Duration", "Classification", "Opening Date");
            Console.WriteLine("{0,-30} {1,-20} {2,-20} {3,-20}", "-----", "--------", "--------------", "------------");
            foreach (var movieDetail in movieList)
            {
                Console.WriteLine(movieDetail.ToString());
            }
        }

        //print the screening of the specfic Movie selected
        public static void printMovieSession(Movie selectedMovie)
        {
            Console.WriteLine("{0,-30} {1,-30} {2,-20} {3,-30} {4,-30} {5,-30}", "Screening Number", "Cinema Name", "HallNo", "Movie Name", "Screening Date Time", "Screening Type");
            Console.WriteLine("{0,-30} {1,-30} {2,-20} {3,-30} {4,-30} {5,-30}", "----------------", "-----------", "------", "----------", "-------------------", "--------------");

            foreach (var screening in selectedMovie.screeningList)
            {
                Console.WriteLine(screening.ToString());
            }
        }

        public static void printScreeningList(List<Screening> screeningList)
        {
            Console.WriteLine("{0,-30} {1,-30} {2,-20} {3,-30} {4,-30} {5,-30}", "Screening Number", "Cinema Name", "HallNo", "Movie Name", "Screening Date Time", "Screening Type");
            Console.WriteLine("{0,-30} {1,-30} {2,-20} {3,-30} {4,-30} {5,-30}", "----------------", "-----------", "------", "----------", "-------------------", "--------------");
            foreach (var screeningDetail in screeningList)
            {
                Console.WriteLine(screeningDetail.ToString());
            }
        }

        public static Movie getMovieName(List<Movie> movieList)  //etither NULL or correct Movie name 
        {
            string movieTitle = "";

            bool status = true;
            //get user input on movie
            while (status)
            {
                //if user enters anything but a string word, print message and ask for user input again
                Console.Write("Enter a movie title: ");
                movieTitle = Console.ReadLine().Trim();  //read the input, trim out any whitespace before or after the string input 

                if (string.IsNullOrWhiteSpace(movieTitle))  //if entered input is a whitespace 
                {
                    Console.WriteLine("Please do not enter a white space.");  //tell the user not to enter a white space 
                    Console.WriteLine("");
                }
                else  //break out of validation loop for white space 
                {
                    status = false;
                }
            }

            foreach (var movie in movieList)
            {
                if (movie.title.ToLower() == movieTitle.ToLower())  //if entered movieTitle from user is among list of movie titles, return the Movie object
                {
                    return movie;
                }
            }
            return null;  //if wrong user input for movieTitle return null
        }

        //populate screening list under each movie 
        public static void populateScreeningListUnderMovie(List<Movie> movieList, List<Screening> screeningList)
        {
            foreach (var movie in movieList)
            {
                foreach (var screening in screeningList)
                {
                    if (screening.movie.title == movie.title)
                    {
                        movie.addScreening(screening);  //each movie has their own list of screenings from list of screening 
                    }
                }
            }
        }

        public static string askForScreeningType()
        {
            string screeningType = "";
            bool status = true;

            //get user input on screening type
            while (status)
            {
                //if user enters anything but a string word, print message and ask for user input again
                Console.Write("Enter a screening type: ");
                screeningType = Console.ReadLine().Trim().ToUpper();

                if (screeningType.All(char.IsWhiteSpace))  //if entered input is a whitespace 
                {
                    Console.WriteLine("Please do not enter a white space.\n");  //tell user not to enter white space
                }
                else  //break out of validation loop
                {
                    status = false;
                }
            }

            if (screeningType == "2D" || screeningType == "3D")  
            {
                return screeningType;
            }
            return null;
        }

        public static DateTime askForScreeningDateForSlectedMovie(Movie selectedMovie)
        {
            var screeningDate = new DateTime();
            string stringInputForScreeningDate;
            bool status = true;

            while (status)
            {
                Console.Write("Enter a screening date for the movie: ");
                stringInputForScreeningDate = Console.ReadLine().Trim();

                if (string.IsNullOrWhiteSpace(stringInputForScreeningDate))
                {
                    Console.WriteLine("Please do not enter a white space.");
                    Console.WriteLine("");
                }

                //keep asking user for exact datetime format day/month/year hour:minute AM/PM
                //used CultureInfo.InvariantCulture since im formatting user's input string that should be parseable by a piece of software independent of the user's local settings
                else if (!DateTime.TryParseExact(stringInputForScreeningDate, "dd/MM/yyyy h:mm tt", CultureInfo.InvariantCulture, DateTimeStyles.None, out screeningDate))
                {
                    Console.WriteLine("Please enter screening date in date time format day/month/year hour:minute AM/PM. day and month have to be double digits.\n");
                }

                else
                {
                    status = false;
                }
            }

            if (screeningDate > selectedMovie.openingDate)  //if new screening date is after opening date of movie, return the screening date since it correct
            {
                return screeningDate;
            }

            else
            {
                screeningDate = default(DateTime);  //else return default time since incorrect screening datetime as user input DateTime has to be after movie's opening date 
            }
            return screeningDate;
        }

        //search and return a cinema from list of cinemas
        public static Cinema selectCinemaHall(List<Cinema> CinemasList)
        {
            string selectedCinema = "";
            bool status = true;

            //while loop to make sure user does not enter a whitespace for cinema hall input 
            while (status)
            {
                Console.Write("\nPlease select a cinema hall: ");
                selectedCinema = Console.ReadLine().Trim();

                if (string.IsNullOrWhiteSpace(selectedCinema))
                {
                    Console.WriteLine("Please do not enter a white space.");
                    Console.WriteLine("");
                }
                else
                {
                    status = false;
                }
            }

            foreach (var cinema in CinemasList)  //get the cinema from user 
            {
                if (cinema.name.ToLower() == selectedCinema.ToLower())
                {
                    return cinema;
                }
            }
            return null;
        }

        //method to get correct hall number of chosen cinema from user (e.g. user chooses Singa North cinema, user can only enter hall numbers from 1 to 3)
        public static int getHallNoOfCinema(Cinema selectedCinema, List<Cinema> cinemaList)
        {
            List<int> hallNumbersList = new List<int>();

            foreach (var cinema in cinemaList)
            {
                if (selectedCinema.name == cinema.name)
                {
                    hallNumbersList.Add(cinema.hallNo);  //list of hall numbers of the specific chosen cinema 
                }
            }

            int chosenHallNumber;

            Console.Write("Enter a hall number from " + hallNumbersList[0] + " to " + hallNumbersList.Last() + ": ");
            while (!int.TryParse(Console.ReadLine(), out chosenHallNumber))
            {
                Console.Write("Please enter a correct integer hall number from " + hallNumbersList[0] + " to " + hallNumbersList.Last() + ": ");
            }

            foreach (var hallNum in hallNumbersList)
            {
                if (hallNum == chosenHallNumber)  //if correct hall number input return the hallNum
                {
                    return hallNum;
                }
            }

            return 0;  //return 0 if user entered wrong hall number
        }

        public static bool checkIfDateTimeCollides(List<Screening> screeningList, DateTime userScreeningDateTime, Cinema selectedCinema, int chosenHallNumber, Movie selectedMovie)
        {
            DateTime userInputTimePlusCleaningAndDuration = new DateTime();
            List<Screening> selectedScreeningList = new List<Screening>();  //list of screening if the name and hall number of selected Cinema from user is same as screening from screeningList
            List<DateTime> newScreeningDateTimeList = new List<DateTime>();  //list of new DateTime after adding 30 mins cleaning time and duration of movie to the old screening DateTime 

            //fetching cinemas with the same screening cinema name and hallNo, so that we can cross check newScreening DateTime of cinema name and hallNo with old screenings of same cinema name and hallNo
            foreach (var screening in screeningList)  
            {
                //if the screening's cinema name and hall number coincide with user selected Cinema's name and hall number 
                if (screening.cinema.name == selectedCinema.name && screening.cinema.hallNo == chosenHallNumber)
                {
                    selectedScreeningList.Add(screening);  //add that screening object to list so that we can check if DateTime of old screenings collide with new Screening DateTime
                    //selectedScreeningList contains list of screenings that was chosen according to the Cinema's name and hall number  
                }
            }

            int durationOfMovieAndCleaning = selectedMovie.duration + 30;  //total mins taken for cleaning cinema and watching movie 

            foreach (var screening in selectedScreeningList)
            {
                //adding cleaning and movie duration to old DateTime of each screening from list of old screenings of the movie
                DateTime afterMovieAndCleaningDateTime = screening.screeningDateTime.AddMinutes(durationOfMovieAndCleaning);
                newScreeningDateTimeList.Add(afterMovieAndCleaningDateTime);
            }

            userInputTimePlusCleaningAndDuration = userScreeningDateTime.AddMinutes(durationOfMovieAndCleaning);


            bool status = true;
            foreach (var screening in selectedScreeningList)  //foreach screening from the specific screening 
            {
                DateTime startTime = screening.screeningDateTime;  //startTime will be starting time of each existing screenings of the movie

                //newScreeningDateTimeList contains ending datetimes after starting time of each existing screening adds on time to clean and duration of movie
                foreach (var endDateTime in newScreeningDateTimeList)  
                {
                    //if user inputs a timing which overlaps between movie starting time and end of movie + cleaning time or 
                    if (!(userInputTimePlusCleaningAndDuration < startTime || userScreeningDateTime > endDateTime))
                    {
                        status = false;
                    }
                }
            }

            //if status is false, means user entered new timing that overlapped with old timing 
            if (status == false)
            {
                return true;
            }

            //else return false since correct new timing 
            return false;
        }

        //create the new screening object to add to screeningList
        public static void createScreeningObject(int chosenhallNumber, Movie selectedMovie, DateTime userScreeningDateTime, string screeningType, List<Screening> screeningList, Cinema selectedCinema, List<Cinema> cinemaList)
        {
            int newScreeningNo = screeningList.Last().screeningNo + 1;  //add 1 to final screening number of list of screenings to increase count index of new screening by one
            selectedCinema.hallNo = chosenhallNumber;  //change to user selected hall number 
            var newScreening = new Screening(newScreeningNo, userScreeningDateTime, screeningType, selectedCinema, selectedMovie);

            //if added new screening under the cinema, that cinema's capacity will be number of seats of the new screening seatsRemaining
            foreach (var cinema in cinemaList)
            {
                if (cinema.name == newScreening.cinema.name && cinema.hallNo == newScreening.cinema.hallNo)
                {
                    newScreening.seatsRemaining = cinema.capacity;
                }
            }

            selectedMovie.addScreening(newScreening);
            screeningList.Add(newScreening);
        }

        public static int getScreeningNumber(List<Screening> screeningList)
        {
            int screeningNo;
            Console.Write("Enter a screening number of a screening session: ");
            while (!int.TryParse(Console.ReadLine().Trim(), out screeningNo))
            {
                Console.Write("Please enter a correct number: ");
            }

            foreach (var screening in screeningList)
            {
                if (screening.screeningNo == screeningNo)
                {
                    return screeningNo;
                }
            }
            return 0;
        }

        public static Screening removeScreeningSession(List<Screening> screeningList, int screeningNoToRemove, List<Movie> movieList)
        {
            //it may not be pointing to an object without "new Screening()"
            var screeningToRemove = new Screening();  //as long as it declared outside for loop, can access

            foreach (var screening in screeningList)
            {
                if (screeningNoToRemove == screening.screeningNo)  //among list of screenings, if chosen screening's number index is that number index of screening list, select that screening to be removed 
                {
                    screeningToRemove = screening;
                    break;
                }
            }

            foreach (var movie in movieList)  //remove the screening of the specific movie
            {
                movie.removeScreening(screeningToRemove);  //screeningToRemove has to be assigned "new Screening()" so that removeScreening class can use this Screening object screeningToRemove
            }

            return screeningToRemove;
        }

        //return true if Movie has more than 0 screenings
        public static bool checkIfMovieStillHaveScreening(Movie selectedMovie)
        {
            if (selectedMovie.screeningList.Count() != 0)
            {
                return true;
            }
            return false;
        }

        //check among list of screenings that there exists a movie screening the user wishes to order
        public static bool checkIfHaveMovieScreening(Movie chosenMovie, List<Screening> screeningList)
        {
            foreach (var screening in screeningList)
            {
                if (chosenMovie.title == screening.movie.title)
                {
                    return true;  //return the true if movie is amongst the list of screening
                }
            }
            return false;  //return false if chosen movie dont exist amongst screening list
        }

        //used to show to user the movies that are available for ordering from list of screenings
        public static List<string> addMovieNamesThatHaveScreening(List<Movie> movieList, List<Screening> screeningList)
        {
            List<string> movieNamesWithScreeningList = new List<string>();
            foreach (var screening in screeningList)
            {
                foreach (var movie in movieList)
                {
                    if (movie.title == screening.movie.title)
                    {
                        movieNamesWithScreeningList.Add(movie.title);
                    }
                }
            }

            //distinct screening list to allow user to chose from list of available movies to order
            List<string> distinctListOfMovieNames = movieNamesWithScreeningList.Distinct().ToList();
            return distinctListOfMovieNames;
        }

        public static Screening getScreeningNumberFromMovieScreening(List<Screening> screeningList, Movie selectedMovie)
        {
            List<Screening> selectedMovieScreeningList = new List<Screening>();
            foreach (var screening in screeningList)
            {
                if (selectedMovie.title == screening.movie.title)
                {
                    selectedMovieScreeningList.Add(screening);  //all screenings under the selected movie 
                }
            }

            int chosenScreeningNumber;
            while (true)
            {
                chosenScreeningNumber = methods.getScreeningNumber(selectedMovieScreeningList);  //get screening number from movie's list of screenings 

                if (chosenScreeningNumber == 0)  //if invalid screening number, print error message 
                {
                    Console.WriteLine("Please enter a correct screening number.\n");
                }
                else
                {
                    break;
                }
            }

            var screeningToBeReturned = new Screening();
            foreach (var screening in screeningList)
            {
                if (screening.screeningNo == chosenScreeningNumber)  //if entered screening number is number from list of screenings, select that screening object to return
                {
                    screeningToBeReturned = screening;
                }
            }
            return screeningToBeReturned;
        }

        public static int getTicketsOrdered(Screening screeningToBeReturned)  //get number of tickets for the single order and return it 
        {
            int totalTickets;
            Console.Write("Enter total number of tickets below " + screeningToBeReturned.seatsRemaining + " to order: ");

            while (!int.TryParse(Console.ReadLine(), out totalTickets))
            {
                Console.Write("Please enter a correct number: ");
            }

            if (totalTickets == 0)  //cannot eneter 0 and number of tickets ordered has to be smaller than seats remaining of that specific screening
            {
                return -1;
            }

            if (screeningToBeReturned.seatsRemaining < totalTickets)
            {
                return 0;
            }

            return totalTickets;
        }

        //check if movie classification is anything but "G"
        public static bool checkMovieClassifications(Movie selectedMovie)
        {
            if (selectedMovie.classification == "G")  //if it is "G", return true
            {
                return true;
            }
            return false;  //return false if it is not rated "G"
        }

        //if it is not "G" rating, have to ask staff for each ticket, do they go above the age requirements
        //if one of the ticket does not reach age requirement, remove the ticket
        public static int askForTicketAge(int totalTickets, Movie selectedMovie)
        {
            string confirmation;
            int ticketRemoved = 0;
            for (int i = 0; i < totalTickets; i++)  //for each ticket, have a while true loop to keep asking if buyer is above the age requirement  
            {
                while (true)
                {
                    int counter = i + 1;
                    Console.Write("Does ticket holder " + counter + " go above the age for classification " + selectedMovie.classification + "? ");
                    confirmation = Console.ReadLine();
                    if (confirmation == "n" || confirmation == "N")  //if n or N means user below age requirement 
                    {
                        Console.WriteLine("This ticket has been removed as the ticket holder is not above age requirement.\n");
                        ticketRemoved = ticketRemoved + 1;
                        break;
                    }
                    else if (confirmation == "Y" || confirmation == "y")
                    {
                        Console.WriteLine("Valid ticket!\n");
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Please enter 'Y'/'y' or 'N'/'n'.\n");
                    }
                }
               
            }
            int remainingTickets = totalTickets - ticketRemoved;  //remainingTickets is number of tickets originally ordered minus all invalid age buyer tickets 
            return remainingTickets;
        }

        public static Order createNewTicktForOrder(int numberOfTickets, Screening chosenScreening, List<Cinema> cinemaList, Movie selectedMovie, List<Order> listOfOrders)
        {
            //brand new order
            double adultTotalPriceToPay = 0;
            double studentPrice = 0;
            double seniorCitizenPrice = 0;
            int seniorCitizenAge;
            string ageGroup;
            string finalStudyLevel = "";
            string levelOfStudy;
            string popcornDealConfirmation;
            var newOrder = new Order();
            var newAdultOrder = new Adult();

            newOrder = new Order((listOfOrders.Count()) + 1, DateTime.Now);
            newOrder.status = "Unpaid";
            for (int i = 0; i < numberOfTickets; i++)
            {
                //validation to check if user entered age group is Student, Adult, Senior Citizen
                while (true)
                {
                    int count = i + 1;
                    Console.Write("Please enter age group (Student, Adult, Senior Citizen) for ticket number " + count + ": ");
                    ageGroup = Console.ReadLine().Trim().ToLower();

                    if (ageGroup == "student" || ageGroup == "adult" || ageGroup == "senior citizen")
                    {
                        break;
                    }
                    else if (string.IsNullOrWhiteSpace(ageGroup))
                    {
                        Console.WriteLine("Please do not enter a white space.\n");
                    }
                    else
                    {
                        Console.WriteLine("Please enter a correct age group.\n");
                    }
                }

                //if ticket holder is a student, check if student level of study is Primary, Secondary, Tertiarys
                if (ageGroup == "student")
                {
                    while (true)
                    {
                        Console.Write("Please enter student's level of study (Primary, Secondary, Tertiary): ");
                        levelOfStudy = Console.ReadLine().Trim().ToLower();

                        if (levelOfStudy == "primary" || levelOfStudy == "secondary" || levelOfStudy == "tertiary")
                        {
                            //Primary students are not allowed to watch NC16 shows etc
                            if (levelOfStudy == "primary" && selectedMovie.classification == "NC16" || levelOfStudy == "primary" && selectedMovie.classification == "M18" || levelOfStudy == "primary" && selectedMovie.classification == "R21" || levelOfStudy == "secondary" && selectedMovie.classification == "M18" || levelOfStudy == "secondary" && selectedMovie.classification == "R21")
                            {
                                Console.WriteLine(levelOfStudy + " level of study is not permitted for " + selectedMovie.classification + ". Please select again.\n");
                            }
                            else
                            {
                                Console.WriteLine("The student ticket has been validated.\n");
                                break;
                            }
                        }
                        else if (levelOfStudy.All(char.IsWhiteSpace))
                        {
                            Console.WriteLine("Please do not enter a whitespace.\n");
                        }
                        else
                        {
                            Console.WriteLine("Please enter a correct level of study. First letter of each word has to be capitalised.\n");
                        }
                    }

                    chosenScreening.seatsRemaining = chosenScreening.seatsRemaining - 1;  ///minus 1 student taken seat from chosen Screening's cinema seats 
                    var newStudentOrder = new Student(chosenScreening, finalStudyLevel);  //create new variable of Student class since theres new Student order
                    studentPrice = newStudentOrder.CalculatePrice();  //use calculatePrice() method to get price of that ordered Student ticket
                    newOrder.amount += studentPrice;  //add the price of the student ticket to order.amount (order.amount is total price of all tickets combined)
                    newOrder.AddTicket(newStudentOrder);  //add a new Student ticket to order's ticket list (so now ticket list consists of one student order)
                }

                //else if ticket holder is a senior citizen
                else if (ageGroup == "senior citizen")
                {
                    Console.Write("Please enter senior citizen's age: ");
                    while (!int.TryParse(Console.ReadLine().Trim(), out seniorCitizenAge))
                    {
                        Console.Write("Please enter a valid age: ");
                    }

                    if (seniorCitizenAge < 55)  //if senior citizen is belowe aged 55, reject the ticket
                    {
                        Console.WriteLine("This senior citizen ticket has been rejected as he or she is not above 55.\n");
                    }

                    //if senior citizen aged 55 and above, approve it, calculate price of senior citizen ticket and add to order's total price 
                    else
                    {
                        Console.WriteLine("Senior Citizen ticket has been validated!\n");
                        chosenScreening.seatsRemaining = chosenScreening.seatsRemaining - 1;  //validate the correct senior age, remove one seats from that screening's cinema 
                        var newSeniorCitizenOrder = new SeniorCitizen(chosenScreening, seniorCitizenAge);
                        seniorCitizenPrice = newSeniorCitizenOrder.CalculatePrice();
                        newOrder.amount += seniorCitizenPrice;
                        newOrder.AddTicket(newSeniorCitizenOrder);
                    }
                }

                //else if ticket holder is an adult
                else if (ageGroup == "adult")
                {
                    bool popcornDealBool;

                    chosenScreening.seatsRemaining = chosenScreening.seatsRemaining - 1;
                    while (true)
                    {
                        Console.Write("Does the ticket holder wish to have the popcorn deal for $3? ");
                        popcornDealConfirmation = Console.ReadLine().Trim().ToUpper();

                        if (popcornDealConfirmation == "Y")
                        {
                            Console.WriteLine("Deal has been added!\n");
                            popcornDealBool = true;
                            newAdultOrder = new Adult(chosenScreening, popcornDealBool);
                            adultTotalPriceToPay = newAdultOrder.CalculatePrice() + 3;  //add 3 dollar to total price of adult ticket order for popcorn
                            break;
                        }
                        else if (popcornDealConfirmation == "N")
                        {
                            Console.WriteLine("Deal has been called off.\n");
                            popcornDealBool = false;
                            newAdultOrder = new Adult(chosenScreening, popcornDealBool);
                            adultTotalPriceToPay = newAdultOrder.CalculatePrice();
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Please enter only 'y' or 'n'.\n");
                        }
                    }

                    newOrder.amount += adultTotalPriceToPay;  //add price of adult ticket to order's total amount 
                    newOrder.AddTicket(newAdultOrder);  //add an adult ticket to order's ticket list (so now thta order has an adult ticket)
                }
            }
            return newOrder;
        }

        //asking user to press any key, type in amount, pay and change order status to paid
        public static double askUserToPayAndReturnMoneyGiven(Order newOrder)
        {
            double amountToPay;
            Console.WriteLine("");
            Console.WriteLine("The amount user has to pay is $" + newOrder.amount);
            Console.Write("Please press any key to pay. ");  //press anything to continue
            Console.ReadKey();

            Console.WriteLine("");
            Console.Write("Please enter amount to pay: ");
            while (!double.TryParse(Console.ReadLine(), out amountToPay))
            {
                Console.Write("Please enter correct amount to pay: ");
            }

            return amountToPay;
        }

        //loop to make sure user gives correct amount. if given amount is more than payable amount, return change 
        public static double checkUserAmountPaid(Order newOrder)
        {
            double amountPaid;
            double changeGiven;
            while (true)
            {
                amountPaid = methods.askUserToPayAndReturnMoneyGiven(newOrder);  //ask for user to input money to pay for order's price  
                if (amountPaid < newOrder.amount)
                {
                    Console.WriteLine("Please enter an amount greater than $" + newOrder.amount);
                }
                else
                {
                    newOrder.status = "Paid";
                    changeGiven = amountPaid - newOrder.amount;
                    Console.WriteLine("The order has been successfully paid for. The change is $" + changeGiven);
                    break;
                }
            }
            return changeGiven;
        }

        //return true if list of orders is empty; checking if list of orders is empty
        public static bool isListOfOrderEmpty(List<Order> listOfOrders)
        {
            if (listOfOrders.Count == 0)
            {
                return true;
            }
            return false;
        }

        public static Order getOrderNumber(List<Order> listOfOrders)  //return true if order number is a valid number from list of orders 
        {
            double orderNumber;
            
            Console.Write("\nPlease enter order number: ");
            while (!double.TryParse(Console.ReadLine(), out orderNumber))
            {
                Console.Write("Please enter a correct order number: ");
            }

            foreach (var order in listOfOrders)
            {
                if (order.orderNo == orderNumber)  
                {
                    return order;  //return the searched order if user input correct order number from list of orders 
                }
            }
            return null;  //return null if incorrect order number index input from user 
        }

        //to remove an order from list of orders, the screening involved has to still exist in list of screenings 
        public static bool checkIfScreeningStillOn(Order selectedOrder, List<Screening> listOfScreenings)
        {
            //return true if screening is still on
            foreach (var ticket in selectedOrder.ticketList)  //have to get the screening from ticket under order's list of tickets 
            {
                foreach (var screening in listOfScreenings)
                {
                    //if the screening under the ticket's screening is the same as one of the screenings from list of screenings (if it exists in list of screening)
                    if (ticket.screening.screeningNo == screening.screeningNo)  
                    {
                        return true;
                    }
                }
            }
            return false;  //return false if order's ticket's screening does not exist anymore, no longer among list of screenings 
        }

        //search for the cinema that needs to have seats updated, update that specific cinema's seat number and return it 
        public static void searchForCinemaToUpdate(List<Cinema> cinemaList, Order selectedOrder, List<Order> listOfOrders, List<Screening> listOfScreening)
        {
            int numberOfTickets;
            var chosenScreening = new Screening();
            
            //when order is cancelled, i have to add back number of seats ordered to order's cinema seatsRemaining 
            foreach (var screening in listOfScreening)
            {
                foreach (var ticket in selectedOrder.ticketList)
                {
                    if (screening.screeningNo == ticket.screening.screeningNo)
                    {
                        chosenScreening = screening;
                    }
                }
            }
            numberOfTickets = selectedOrder.ticketList.Count();  //number of tickets is number of seats to be added back since tickets ordered is seats taken
            chosenScreening.seatsRemaining += numberOfTickets;  //add back seats that were removed from the order to the order's screening cinema capacity
            selectedOrder.status = "Cancelled";
            listOfOrders.Remove(selectedOrder);
            Console.WriteLine("This order has been refunded.\n");
        }

        public static void recommendMovie(List<Order> listOfOrders, List<Movie> movieList)
        {
            int maxNumberOfTickets;
            string nameOfMovie = "";
            List<int> numberOfTicketsList = new List<int>();
            var mostPopularMovie = new Order();

            foreach (var order in listOfOrders)  //foreach order in list of orders, 
            {
                numberOfTicketsList.Add(order.ticketList.Count);  //add number of tickets of each order from list of orders to a list
            }

            //to find most popular movie, tickets sold has to be highest amongst list of number of tickets sold 
            maxNumberOfTickets = numberOfTicketsList.Max();  //max number of tickets from list of tickets is most number of tickets sold 

            for (int i = 0; i < numberOfTicketsList.Count; i++)  //index of both numberOfTicketsList and listOfOrders are the same 
            {
                if (numberOfTicketsList[i] == maxNumberOfTickets)  //if number of ticket in numberOfTicketsList is most tickets sold,
                {
                    mostPopularMovie = listOfOrders[i];  //assign that specific order with most number of tickets to var mostPopularMovie
                }
            }

            foreach (var ticket in mostPopularMovie.ticketList)
            {
                nameOfMovie = ticket.screening.movie.title;  //retrieve name of movie of most tickets sold
            }

            Console.WriteLine("Since the last order at " + mostPopularMovie.orderDateTime + ", the most popular movie with " + maxNumberOfTickets + " tickets sold is " + nameOfMovie + ".");
        }

        //populat the seats remaining for each screening's seatsRemaining 
        public static void populateSeatsRemaining(List<Screening> screeningList, List<Cinema> cinemaList)
        {
            foreach (var cinema in cinemaList)
            {
                foreach (var screening in screeningList)
                {
                    if (cinema.name == screening.cinema.name && cinema.hallNo == screening.cinema.hallNo)
                    {
                        screening.seatsRemaining = cinema.capacity;
                    }
                }
            }
        }

        public static void recommendTopThreeMovie(List<Order> listOfOrders, List<Movie> movieList)
        {
            int maxNumberOfTickets;
            int minNumberOfTickets;
            int midNumberOfTickets = 0;
            string nameOfPopularMovie = "";
            string nameOfLeastPopularMovie = "";
            string nameOfMidPopularMovie = "";
            var mostPopularMovie = new Order();
            var midPopularMovie = new Order();
            var leastPopularMovie = new Order();
            List<int> numberOfTicketsList = new List<int>();

            #region placing amount spent on each movie in a list of amounts
            //List<double> listOfAmounts = new List<double>();
            //foreach (var order in listOfOrders)
            //{
            //    listOfAmounts.Add(order.amount);
            //} 
            #endregion

            foreach (var order in listOfOrders)
            {
                //can use max, min and "<",">" to find most popular, least popular and middle range movie 
                numberOfTicketsList.Add(order.ticketList.Count);  //add number of tickets sold of the order to a list of ticket numbers 
            }

            #region getting first, second, third popular movies
            ////sorting list of ticket numbers from small to big 
            ////order by needs an expression so "v" is used as expression in "v => v"
            ////.OrderBy is an enumerable, so .ToList() materializes the enumerable back to a list since we using .OrderBy to sort a list of int
            //var sortedTicketNumbers = numberOfTicketsList.OrderBy(v => v).ToList();  

            //int secondLast = sortedTicketNumbers.Count() - 1;  //second last is one before last
            //int thirdLast = sortedTicketNumbers.Count() - 2;  //third last is 2 before last

            //foreach (var order in listOfOrders)
            //{
            //    if (listOfAmounts.Last() == order.amount)  //final ticket number in list is most tickets sold
            //    {
            //        mostPopularMovie = order;
            //    }
            //    if (listOfAmounts[secondLast - 1] == order.amount)  //index in coding is 1 before orignial 
            //    {
            //        midPopularMovie = order;
            //    }
            //    else if (listOfAmounts[thirdLast - 1] == order.amount)
            //    {
            //        leastPopularMovie = order;
            //    }
            //}
            #endregion

            maxNumberOfTickets = numberOfTicketsList.Max();  //max number of tickets from list of tickets is most number of tickets sold 
            minNumberOfTickets = numberOfTicketsList.Min();  //min number of tickets from list of tickets is min number of tickets sold 

            for (int i = 0; i < numberOfTicketsList.Count; i++)  //count of both numberOfTicketsList and listOfOrders are the same 
            {
                if (numberOfTicketsList[i] == maxNumberOfTickets)  //if number of ticket in numberOfTicketsList is most tickets sold,
                {
                    mostPopularMovie = listOfOrders[i];  //assign that specific index of listOfOrders to mostPopularMovie var since listOfOrders[i] is the order that sold most number of tickets 
                }
                else if (numberOfTicketsList[i] == minNumberOfTickets)  //if number of ticket in numberOfTicketsList is minimum tickets sold,
                {
                    leastPopularMovie = listOfOrders[i];
                }

                else if (numberOfTicketsList[i] > minNumberOfTickets && numberOfTicketsList[i] < maxNumberOfTickets)  //middle range ticket sold is greater than min ticket, lesser than max tickets sold 
                {
                    midPopularMovie = listOfOrders[i];
                    midNumberOfTickets = midPopularMovie.ticketList.Count();
                }
            }

            foreach (var ticket in mostPopularMovie.ticketList)
            {
                nameOfPopularMovie = ticket.screening.movie.title;  //retrieve name of movie of most tickets sold 
            }

            foreach (var ticket in leastPopularMovie.ticketList)
            {
                nameOfLeastPopularMovie = ticket.screening.movie.title;
            }

            foreach (var ticket in midPopularMovie.ticketList)
            {
                nameOfMidPopularMovie = ticket.screening.movie.title;
            }

            Console.WriteLine("Since the last order at " + mostPopularMovie.orderDateTime + ", the most popular movie with " + maxNumberOfTickets + " tickets sold is " + nameOfPopularMovie + ".\n");
            Console.WriteLine("Since the last order at " + midPopularMovie.orderDateTime + ", the middle popular movie with " + midNumberOfTickets + " tickets sold is " + nameOfMidPopularMovie + ".\n");
            Console.WriteLine("Since the last order at " + leastPopularMovie.orderDateTime + ", the least popular movie with " + minNumberOfTickets + " tickets sold is " + nameOfLeastPopularMovie + ".");

            //Console.WriteLine("Since the last order at " + mostPopularMovie.orderDateTime + ", the most popular movie with " + mostPopularMovie.ticketList.Count + " tickets sold is " + nameOfPopularMovie + ".\n");
            //Console.WriteLine("Since the last order at " + midPopularMovie.orderDateTime + ", the middle popular movie with " + midPopularMovie.ticketList.Count + " tickets sold is " + nameOfMidPopularMovie + ".\n");
            //Console.WriteLine("Since the last order at " + leastPopularMovie.orderDateTime + ", the least popular movie with " + leastPopularMovie.ticketList.Count + " tickets sold is " + nameOfLeastPopularMovie + ".");
        }
    }
}