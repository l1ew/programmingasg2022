﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//============================================================
// Student Number : S10222254, S10221765
// Student Name : Liew Zheng Zhang, Koay Yi Ting
// Module Group : P06
//============================================================

namespace final_prg_asg_2022
{
    class Cinema
    {
        public string name { get; set; }  //get; set; --> properties, properties should be first letter capitalised
        public int hallNo { get; set; }
        public int capacity { get; set; }

        public Cinema()
        {
        }

        //parameters should be small letters
        public Cinema(string Name, int HallNo, int Capacity)
        {
            name = Name;
            hallNo = HallNo;
            capacity = Capacity;
        }

        public override string ToString()
        {
            return string.Format("{0,-20} {1,-20} {2,-20}", name, hallNo, capacity);
        }
    }
}
