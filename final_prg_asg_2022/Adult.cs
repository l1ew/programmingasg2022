﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//============================================================
// Student Number : S10222254, S10221765
// Student Name : Liew Zheng Zhang, Koay Yi Ting
// Module Group : TP06
//============================================================


namespace final_prg_asg_2022
{
    class Adult : Ticket
    {
        //a constructor makes sure that your object is in "a valid state" when they are made
        //e.g. int default is 0, cannot be null. But string can be null.
        //thus i have to decide if having a null value is valid or not. if its not, then all constructors should populate it 
        //constructors are usually never null, so class constructor MUST ensure they are not null, thus making them in "valid state"
        //example, if an int value is only 1-10, constructor should enforce that
        public bool popcornOffer { get; set; }

        //base as a keyword is how we invoke the parent class constructor 
        //since base class is Ticket here, any valid Adult MUST also be a valid Ticket
        //however, withing Adult class it does not know what "valid Ticket" means, thus must call constructor of Ticket to know what is "valid"
        //through the use of ":base()" where within :base parameters will be objects stated in base class Ticket
        public Adult() :base()
        { }

        //base class is Ticket, where Adult class inherits from Ticket class
        //if have parameters in base class, :base() will have objects in parameter (e.g. :base(screeningObject))
        //base class constructor has a Screening class object, thus Adult class inherits base with Screening screening as constructor
        public Adult(Screening screening, bool PopcornOffer) : base(screening)
        {
            popcornOffer = PopcornOffer;
        }

        //calculate what 
        public override double CalculatePrice()
        {
            double priceToPay = 0;
            DateTime todayDate = DateTime.Now;
            string typeOfScreening = screening.screeningType;  //2D or 3D screening type

            if (typeOfScreening == "3D")
            {
                if (todayDate.DayOfWeek >= DayOfWeek.Monday && todayDate.DayOfWeek <= DayOfWeek.Thursday)  //from monday to thursday
                {
                    priceToPay = 11;
                }
                else
                {
                    priceToPay = 14;
                }
            }
            else if (typeOfScreening == "2D")
            {
                if (todayDate.DayOfWeek >= DayOfWeek.Monday && todayDate.DayOfWeek <= DayOfWeek.Thursday)  //from monday to thursday
                {
                    priceToPay = 8.5;
                }
                else
                {
                    priceToPay = 12.5;
                }
            }
            return priceToPay;
        }

        public override string ToString()
        {
            //base.ToString() will always print the inherited adult class ToString(). for this Adult inherited Ticket, so base.ToString() in Adult will print ToString() of Ticket
            return string.Format("{0,-20}", popcornOffer/*, base.ToString()*/);
        }
    }
}
