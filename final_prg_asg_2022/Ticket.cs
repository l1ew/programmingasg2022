﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//============================================================
// Student Number : S10222254, S10221765
// Student Name : Liew Zheng Zhang, Koay Yi Ting
// Module Group : P06
//============================================================

namespace final_prg_asg_2022
{
    abstract class Ticket
    {
        public Screening screening { get; set; }

        public Ticket() { }

        public Ticket(Screening Screening)
        {
            screening = Screening;
        }

        abstract public double CalculatePrice();

        

        public override string ToString()
        {
            return string.Format("{0,-20}", screening.screeningNo);
        }
    }
}
