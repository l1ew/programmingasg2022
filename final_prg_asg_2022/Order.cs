﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//============================================================
// Student Number : S10222254, S10221765
// Student Name : Liew Zheng Zhang, Koay Yi Ting
// Module Group : P06
//============================================================

namespace final_prg_asg_2022
{
    class Order
    {
        public int orderNo { get; set; }  //starts from 1
        public DateTime orderDateTime { get; set; }
        public double amount { get; set; }
        public string status { get; set; }  //unpaid(when new order is created), Paid(after payment is made), Cancelled
        public List<Ticket> ticketList { get; set; } = new List<Ticket>();

        public Order() { }

        public Order(int OrderNo, DateTime OrderDateTime)
        {
            orderNo = OrderNo;
            orderDateTime = OrderDateTime;
        }

        public void AddTicket(Ticket ticketToBeAdded)
        {
            ticketList.Add(ticketToBeAdded);
        }

        public override string ToString()
        {
            return string.Format("{0,-20} {1,-30} {2,-20} {3,-20}", orderNo, orderDateTime, amount, status);
        }
    }
}
